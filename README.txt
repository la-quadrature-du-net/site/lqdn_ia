Ce module est là pour fournir une page d'administration assez simple avec certains liens.

Le module est composé de 4 fichiers : 
-- lqdn_ia.css
        CSS de la page. Rien de particulier

-- lqdn_ia.install
        Un grand classique des modules Drupal. Il est vide.

-- lqdn_ia.info
        Un grand classique des modules Drupal. Il contient le strict nécessaire. Voici la doc des .info :  
        http://drupal.org/node/171205

-- lqdn_ia.module
        C'est le fichier principal. Il utilise deux hook : 
         -- hook_perm
		http://api.drupal.org/api/function/hook_perm/6
                Permet de définir des permissions.                               
                La permission est la suivante : "Accès à la page d'admin de lqdn"
		Une nouvelle permission apparait dans la page : admin/user/permissions 
		et il faut la donner à certains rôles (dev, ...).
        -- hook_menu
		http://api.drupal.org/api/function/hook_menu/6
                Permet de définir une page (et en option, de mettre un lien dans le menu).
		La page est la suivante : admin/lqdn
		La fonction appelée est la suivante : _lqdn_ia_page
		Toutes les autres fonctions sont appelées par la fonction _lqdn_ia_page

	Le reste, c'est du HTML, sprintf, et des requetes SQL pour trouver des nombres. Rien de particulier.



1- Il faut extraire le module dans ./sites/all/modules/
2- Il faut activer le module
3- Il faut s'assurer que le bloc avec un lien vers la page d'admin s'affiche uniquement pour les rôles concernés
4- Il faut donner l'accès à la page d'admin aux rôles concernés.

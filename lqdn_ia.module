<?php
/**
 * @file
 * lqdn_ia module.
 *
 * @ingroup lqdn
 *
 */

/**
 * Implements hook_permission().
 *
 * Defines new permissions.
 */
function lqdn_ia_permission() {
    return array(
        'access lqdn_ia full content' => array(
            'title' => t('Access to LQDN administration page'),
        )
    );
}

/**
 * Implements hook_block_info().
 *
 * Add some blocks
 *
 */
function lqdn_ia_block_info() {
    $blocks[0] = array(
        'info' => t('Link to administration page'), //Lien vers la page d'administration
        'cache' => 'BLOCK_CACHE_GLOBAL',
        'weight' => 0,
        'status' => 1,
    );
    return $blocks;
}

/**
 * Implementation of hook_menu().
 *
 * Prépare des liens
 */
function lqdn_ia_menu() {
    $items['admin/lqdn'] = array(
        'title' => t('La Quadrature'),
        'page callback' => '_lqdn_ia_page',
        'page arguments' => array(),
        'access arguments' => array('access lqdn_ia'),
        'type' => MENU_NORMAL_ITEM,
        'weight' => -50,
    );

    return $items;
}

/**
 * Implements hook_block_view().
 *
 * Prepares the contents of the block.
 */
function lqdn_ia_block_view($delta = '') {
    if ($delta=='') {
        $block = array(
            'subject' => '',
            'content' => '<p style="text-align:center;"><strong>' . l(t('Site Administration'), 'admin/lqdn') . '</strong></p>',
        );
        return $block;
    }
}

/**
 * Fonction interne
 *
 * Présente le contenu de la page d'administration
 */
function _lqdn_ia_page() {
    global $user; // For "My drafts"

    // Ajout d'une feuille de style spécifique
    drupal_add_css(drupal_get_path('module', 'lqdn_ia') . '/lqdn_ia.css');

    $html_block = '<div class="w50"><h2>%s</h2>%s</div>';

    //--
    //-- Premier bloc, « Adding content » / « Ajout de contenu »
    //--
    $content = '';

    $news = l(t("Quad'news"), 'node/add/actualite');
    $page = l(t('Page'), 'node/add/page');  
    $quote = l(t('Quote'), 'node/add/citations');
    $content .= '<div class="nouveau-contenu important">' . $news . $page . $quote . '</div>';

    $rp = l(t('Press review'), 'node/add/revue-de-presse');
    $position = l(t('Soutien'), 'node/add/soutien');
    $important = l(t('Important'), 'node/add/important');
    $content .= '<div class="nouveau-contenu">' . $rp . $position . $important . '</div>';

    $add_personality = l(t('Add personality'), 'admin/content/taxonomy/5/add/term'); //Ajouter une personnalité
    $themes = l(t('Themes list'), 'admin/content/taxonomy/2'); //Liste des thèmes
    $add_theme = l(t('Add theme'), 'admin/content/taxonomy/2/add/term'); //Ajouter un thème
    $content .= '<p class="lien-final">→ ' . $add_personality . '<br >→ ' . $themes . '<br >→ ' . $add_theme . '</p>';

    $block[0] = sprintf($html_block, t('Adding content'), '<div>' . $content . '</div>');

    //--
    //-- Second bloc, « Drafts » / « Les brouillons »
    //--
    $content = '';

    $data = db_query("SELECT COUNT(*) as 'nb' FROM `node` WHERE status='0'")->fetch();
    $nb_drafts = $data->nb;

    $data = db_query("SELECT COUNT(*) as 'nb' FROM `node` WHERE status='0' AND uid=:uid", array(':uid'=>$user->uid))->fetch();
    $nb_my_drafts = $data->nb;

    $drafts = l(t('Drafts') . ' ('.$nb_drafts.')', 'brouillons');
    $my_drafts = l(t('My drafts') . ' ('.$nb_my_drafts.')', 'brouillons/liste/all/' . $user->name);

    $content_administration = l(t('Content administration'), 'admin/content/node');
    $recent_posts = l(t('Recent posts'), 'tracker');

    $content = '<div class="brouillons">' . 
    $drafts . '</div><div class="brouillons important">' . 
    $my_drafts . '</div><p class="lien-final">→ ' . 
    $content_administration . '<br />→ ' .
    $recent_posts . '</p>';

    $block[1] = sprintf($html_block, t('Drafts'), '<div>' . $content . '</div>');

    //--
    //-- Troisième bloc, « Advanced administration » / « Administration avancée »
    //--
    $content = '';
    $list = array();
    $list[] = l(t('Views'), 'admin/build/views');
    $list[] = l(t('Menus'), 'admin/build/menu');
    $list[] = l(t('Blocks'), 'admin/build/block');
    $list[] = l(t('Translate interface'), 'admin/build/translate/search');
    $list[] = l(t('Modules'), 'admin/build/modules');
    $list[] = l(t('Panels'), 'admin/build/panels');
    $list[] = l(t('Reports'), 'admin/reports');
    foreach ($list as $link) {
        $content .= '<li>' . $link . '</li>';
    }
    $block[2] = sprintf($html_block, t('Advanced administration'), '<ul>' . $content . '</ul>');

    //--
    //-- Quatrième bloc, « Panels » / « Panneaux »
    //--
    $content = '';
    $list = array();
    $list[] = '<strong>' . l(t('Panels'), 'admin/build/panels') . '</strong>';
    $list[] = l('Accueil', 'admin/build/pages/edit/page-accueil');
    $list[] = l('Soutien', 'admin/build/pages/edit/page-soutien2011');
    $list[] = l('HADOPI', 'admin/build/pages/edit/page-HADOPI');
    $list[] = l('Telecoms Package', 'admin/build/pages/edit/page-Telecoms_Package');
    $list[] = l('Filtrage du Net (anc. LOPPSI)', 'admin/build/pages/edit/page-LOPPSI');
    $list[] = l('Net Neutrality', 'admin/build/pages/edit/page-Net_neutrality');
    $list[] = l('ACTA', 'admin/build/pages/edit/page-ACTA');
    $list[] = l('Directive anti-partage IPRED (anc. Rapport Gallo)', 'admin/build/pages/edit/page-Gallo_Report');
    $list[] = l('Information Society Services Directive', 'admin/build/pages/edit/page-internet_directive');
    $list[] = l('Propositions (anc. Financement mutualisé)', 'admin/build/pages/edit/page-financement_mutualise');
    $list[] = '<em>→ ' . l('Liste des panneaux', 'admin/build/pages') . '</em>';
    foreach ($list as $link) {
        $content .= '<li>' . $link . '</li>';
    }
    $block[3] = sprintf($html_block, t('Panels'), '<ul>' . $content . '</ul>');

    return '<div id="lqdn_ia">' . $block[0] . $block[1] . '<div style="clear:both;"></div>' . $block[2] . $block[3] . '</div>';
}
